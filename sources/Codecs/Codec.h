#pragma once

#include "ffmpeg.h"

#include "OpenCodec.h"

namespace ffmpegcpp
{

	class Codec
	{
	public:

		Codec(std::string codecName);
		Codec(AVCodecID codecId);
		virtual ~Codec();

		void SetOption(std::string name, std::string value);
		void SetOption(std::string name, int value);
		void SetOption(std::string name, double value);

		void SetGenericOption(std::string name, std::string value);

		void SetGlobalContainerHeader(); // used by the Muxer for configuration purposes

	protected:

		AVCodecContext* codecContext = nullptr;

		OpenCodec* Open();

	private:

		void CleanUp();

		AVCodecContext* LoadContext(AVCodec* codec);

		bool opened = false;
	};
}
