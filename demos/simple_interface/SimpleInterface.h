// MathLibrary.h - Contains declarations of math functions
#pragma once

#ifdef __linux__
#define DllExport
#else
#define DllExport __declspec(dllexport)
#endif

#include <string>

extern "C" DllExport void* ffmpegCppCreate(const char* outputFileName);

extern "C" DllExport void ffmpegCppAddVideoStream(void* handle, std::string videoFileName);
extern "C" DllExport void ffmpegCppAddAudioStream(void* handle, std::string audioFileName);

extern "C" DllExport void ffmpegCppAddVideoFilter(void* handle, std::string filterString);
extern "C" DllExport void ffmpegCppAddAudioFilter(void* handle, std::string filterString);

extern "C" DllExport void ffmpegCppGenerate(void* handle);

extern "C" DllExport bool ffmpegCppIsError(void* handle);
extern "C" DllExport const char* ffmpegCppGetError(void* handle);

extern "C" DllExport void ffmpegCppClose(void* handle);